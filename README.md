# Projeto 1 - FSE 2020.2

## Resumo

<p align="justify">&emsp;&emsp;O projeto consiste em um sistema de monitoramento e gerenciamento da temperatura interna de uma RaspberryPi.</p>

<p align="justify">&emsp;&emsp;O sistema tentará manter a temperatura interna do dispositivo próxima a uma temperatura de referência definida, fazendo uso de uma ventoinha para resfriamento ou um resistor para aquecimento, conforme necessário.</p>

## Compilação

<p align="justify">&emsp;&emsp;Para gerar um arquivo executável, basta utilizar o comando <code>make</code> na raíz do projeto, onde está localizado o arquvio Makefile. Um arquivo executável <code>bin</code> será gerado na pasta bin.</p>

## Uso

<p align="justify">&emsp;&emsp;Ao iniciar o programa, será apresentado um menu ao usuário. As informações de temperatura e valor de acionamento da ventoinha/resistor são atualizadas periodicamente.</p>

<p align="justify">&emsp;&emsp;O menu pode ser controlado utilizando as setas do teclado. Pressione a tecla Enter para selecionar a opção destacada.</p>

* Selecionar a primeira opção apresentará um campo de input pra o usuário. Digite o valor desejado e pressione Enter.
* Selecionar a segunda opção atualizará a temperatura de referência para o potenciômetro.
* Selecionar a terceira opção encerrará as comunicações e o programa.

<p align="justify">&emsp;&emsp;Atente-se ao intervalo de temperatura permitido na primeira opção. Não é possível inserir temperaturas maiores que 100 ºC ou menores que a temperatura ambiente. O programa irá notificar o usuário caso o intervalo não seja respeitado e reverterá para o potenciômetro.</p>

## Resultados

<p align="justify">&emsp;&emsp;Abaixo estão os resultados da execução do programa por um período de 10 minutos.</p>

### Variação de Temperatura

![Img1](/img/temperature_var.png)

### Sinal do Acionador

![Img2](/img/actuator_var.png)

<p align="justify">&emsp;&emsp;O resistor é acionado assim que o valor lido é superior à 0%. Já a ventoinha é acionada somente em valores inferiores à -40%. Os dispositivos nunca são acionados simultaneamente.</p>

### Observações

<p align="justify">&emsp;&emsp;É recomendado maximizar o terminal antes de executar o programa para evitar problemas na apresentação do menu.</p>

### Referências

[Ncurses](https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/)

[Enunciado do Projeto](https://gitlab.com/fse_fga/projetos_2020_2/projeto-1-2020.2)
